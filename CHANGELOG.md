# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
...

## [2.2.0] - 2019-02-07
- Released to composer

## [2.1.1] - 2018-06-22
### Added
- Added extra fields to Hellodialog product feed

## [2.1.0] - 2018-05-16
### Added
- Batch order export to Hellodialog

## [2.0.0] - 2018-04-13
### Added
- Brand refactoring
- Compatibility updates for release Hellodialog Transactional Emails release