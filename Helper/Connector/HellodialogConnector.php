<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Helper\Connector;

use Zend\Http\Client;
use Zend\Http\Headers as ZendHeaders;
use Zend\Http\Request as ZendRequest;
use Zend\Http\Client as ZendClient;
use Zend\Http\ClientFactory as ZendClientFactory;
use Zend\Http\Response as ZendResponse;

use Hellodialog\Base\Model\Logger;

class HellodialogConnector {
	protected $headers;
	protected $request;
	protected $client;
	protected $response;

	private $token;
	private $endpoint;
	private $useDefaultParams;
	private $useDefaultHeaders;
	private $useDefaultOptions;

	private $logger;

	/**
	 * Constructor
	 *
	 * @param \Zend\Http\Headers  $headers
	 * @param \Zend\Http\Request  $request
	 * @param \Zend\Http\ZendClientFactory   $clientFactory
	 * @param \Zend\Http\Response $response
	 */
	public function __construct(
		ZendHeaders $headers,
		ZendRequest $request,
        ZendClientFactory $clientFactory,
		ZendResponse $response,
        Logger $logger
	)
	{
		$this->headers = $headers;
		$this->request = $request;
		/** @var ZendClient client */
		$this->client = $clientFactory->create();
		$this->response = $response;
		$this->logger = $logger;
    }

	public function init($token, $endpointUrl, $useDefaultParams = true, $useDefaultHeaders = true, $useDefaultOptions = true)
	{
		$this->token = $token;
		$this->endpoint = $endpointUrl;
		$this->useDefaultParams = $useDefaultParams;
		$this->useDefaultHeaders = $useDefaultHeaders;
		$this->useDefaultOptions = $useDefaultOptions;
    }

	public function get($id = null, $params = [])
	{
		$id = ($id !== null) ? "/$id" : '';

		$url   	= $this->endpoint.$id;
		$method = ZendRequest::METHOD_GET;

		$this->doRequest($url, $method, null, $params);

		return $this->response;
	}

	public function post($requestBody)
	{
        $url   	 = $this->endpoint;
		$method  = ZendRequest::METHOD_POST;
		$options = [
			'curloptions' => [
				CURLOPT_POSTFIELDS => json_encode($requestBody), // '[{"email":"gydo@zeo.nl","_state":"Contact"},{"email":"henk@zeo.nl","_state":"Optin"}]',
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_RETURNTRANSFER => true
			]
		];
		$this->doRequest($url, $method, $options);

		return $this->response;
	}

	public function put($hdId, $requestBody) // same as post, but requestbody should contain id param
	{
		$url   	 = "$this->endpoint/$hdId";
		$method  = ZendRequest::METHOD_PUT;
		$options = [
			'curloptions' => [
				CURLOPT_POSTFIELDS => json_encode($requestBody), // '[{"email":"gydo@zeo.nl","_state":"Contact"},{"email":"henk@zeo.nl","_state":"Optin"}]',
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_RETURNTRANSFER => true
			]
		];
		$this->doRequest($url, $method, $options);

		return $this->response;
	}

    public function getEndpoint()
    {
        return $this->endpoint;
	}

	private function doRequest($url, $method, $options = [], $params = [], $headers = [])
	{
		if($this->token && $this->endpoint) { # TODO: No error given, but response is null
			$this->prepareHeaders( $headers );
			$this->prepareRequest( $url, $method, $params );
			$this->prepareClientOptions( $options );

			$this->response = $this->client->send();
            $this->client->getRequest();
		} else {
			$statusCode = 403;
			$responseJsonData = '{ "result": { "status": "ERROR", "message": "(#0) Request not executed, required data not set", "code": 0 } }';

			$this->response->setStatusCode( $statusCode );
			$this->response->setContent( $responseJsonData );
			$this->response->setCustomStatusCode( 0 );
			$this->response->setReasonPhrase( "Request not executed, required data not set" );
		}
	}

	private function prepareHeaders($customHeaders = []) {
		$headers = [
			'Accept' => 'application/json',
			'Content-Type' => 'application/json'
		];

		if( !empty( $customHeaders ) ) {
			if( !$this->useDefaultHeaders ) {
				$headers = $customHeaders;
			} else {
				$headers = array_merge( $headers, $customHeaders );
			}
		}

		$this->headers->addHeaders( $headers );

		$this->client->setHeaders( $this->headers );
	}

	private function prepareRequest($url, $method, $customParams) {
		$this->request->setUri( $url );
		$this->request->setMethod( $method );

		$params = [
			'token' => $this->token
		];

		if( !empty( $customParams ) ) {
			if( !$this->useDefaultParams ) {
				$params = $customParams;
			} else {
				$params = array_merge( $params, $customParams );
			}
		}
		$params = new \Zend\Stdlib\Parameters( $params );

		$this->request->setQuery( $params );

		$this->client->setRequest( $this->request );
	}

	private function prepareClientOptions($customOptions = []) {
		$options = [
			'adapter'   => 'Zend\Http\Client\Adapter\Curl',
			'curloptions' => [
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_RETURNTRANSFER => true
			],
			'maxredirects' => 0,
			'timeout' => 30
		];

		if( !empty($customOptions) ) {
			if(!$this->useDefaultOptions) {
				$options = $customOptions;
			} else {
				$options = array_merge($options, $customOptions);
			}
		}

		$this->client->setOptions($options);

		return $options;
	}
}