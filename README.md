[![N|Solid](https://www.hellodialog.com/wp-content/uploads/2017/01/logo-grijs.png)](https://hellodialog.com/)

_---------------------------------------------_
_Scroll down to the bottom for the english version._
_---------------------------------------------_

# Vergroot je succes via conversie met e-mailmarketing en koppel je webshop met onze Magento 2.1 module.

## Voordat je begint

  - Controleer de versie van je Magento 2 webshop, deze module ondersteunt de Magento 2.1.x versies. Het versienummer is rechtsonder in het admin paneel van een Magento 2 webshop te vinden.
  - Installeer de module altijd eerst op een test omgeving om eventuele conflicten met eerder geïnstalleerde modules te voorkomen.

## Installatie

Loop de volgende stappen in de genoemde volgorde door om de module in je Magento 2.1.x te installeren:
1. Sleep de volledige inhoud van het gedownloade .zip bestand in de root map van de Magento 2.1.x installatie. U kunt de root map herkennen door de aanwezigheid van de app, pub en var mappen.
2. Open je terminal naar keuze en ssh naar de (test) server. Voer vervolgens de volgende commando's uit in de root map van de Magento 2.1.x installatie.

    ```sh
    $ php bin/magento module:enable Hellodialog_Base
    $ php bin/magento maintenance:enable
    $ php bin/magento setup:upgrade
    $ php bin/magento setup:di:compile
    $ php bin/magento setup:static-content:deploy # afhankelijk van de talen van de webshop kan je dit commando meerdere malen uitvoeren voor de betreffende taal
    $ php bin/magento cache:clean
    $ php bin/magento cache:flush
    $ php bin/magento indexer:reindex
    $ php bin/magento maintenance:disable
    ```

3. Navigeer naar de admin van de betreffende webshop en klik door naar STORES > CONFIGURATION
4. Klik onder de tab Hellodialog op Advanced. Hier verschijnen alle velden die nodig zijn voor de koppeling tussen de Magento installatie en Hellodialog. 
    - De velden: "Api Key", "Api Secret" en "Request Url". Het "Request Url" veld hebben we standaard al voor je ingevuld met de waarde "https://app.hellodialog.com/api". Dit kun je zo laten en hoeft alleen onder speciale condities aangepast te worden.
5. Open een nieuwe tab, navigeer naar https://app.hellodialog.com/account/login en log in.
6. Dit brengt je op je Hellodialog dashboard. Aan de linkerzijde bevindt zich een verticaal menu. Klik hierin op "Settings" of "Instellingen" afhankelijk van je taal.
7. Nu ben je aangekomen op de "General Settings" pagina. Onder de titel bevindt zich een horizontaal menu. Klik hierin op "API".
8. Indien je nog niet eerder een API key hebt aangemaakt, klik op "Add key", anders ga door naar stap 11.
9. Er opent zich een dialoogvenster met een aantal velden. Vul deze als volgt in:
    - Description: "Magento {Je magento versie, 1 of 2}"
    - Access: ALL ALL
    - webhook-url: {www.domein.nl}/hellodialog
    - IP-restriction: {Als je een gevorderde gebruiker bent:} Restricted (secure) {anders:} Unrestricted (insecure)
    - IP-address: {Dit veld verschijnt alleen als je bij IP-restriction "secure" hebt aangegeven} Het IP-address van de server waar de requests vandaan gaan komen.
10. Klik op "Save", je ziet nu dat er een Api Key is aangemaakt.
11. Kopieer de waarde onder het label "Key" en plak deze in het "Api Key"-veld in de Magento configuratie.
12. In dezelfde regel als de waarde van de "key" die je zojuist gekopieerd hebt vind je aan de rechterzijde een vraagteken icoon. Hover over dit vraagteken en er zal een tekstballon openen met een "Secret" erin. 
13. Kopieer de waarde onder de tekst "Secret:" en plak deze in het "Secret Key"-veld in de Magento configuratie.
14. Tot slot kun je de module verder configureren door de Hellodialog velden te mappen aan de Magento velden in de "General" tab. 
    - BELANGRIJK: Het is mogelijk dat de standaardwaarden die hier ingevuld zijn onjuist zijn, sinds deze taal afhankelijk zijn. 
    Het is aangeraden deze te controleren met de waarden van de velden in Hellodialog. Je kunt de velden vinden door naar het url: https://app.hellodialog.com/contacts/fields te gaan of via het verticale menu van Hellodialog op "Contacten" te klikken gevolgd door "Velden" in het horizontale menu.
    
## Extra informatie
### Sync Contacts
De Hellodialog Magento 2 module maakt het mogelijk om contacten vanuit Hellodialog naar Magento 2 te halen. 
Dit houdt in dat Magento 2 geüpdatet wordt met contacten uit Hellodialog.
Er worden in dit proces geen contacten verwijderd alleen nieuwe contacten toegevoegd en geüpdatet aan de Magento 2 kant. Dit gebeurt op basis van het email adres en status van inschrijving.

### Export Contacts
Naast de zogenoemde "Sync Contacts" functie bestaat er ook "Export Contacts". 
Deze functie heeft als doel Magento 2 subscribers te exporteren om vervolgens handmatig in Hellodialog te importeren.
Wanneer je binnen de Hellodialog omgeving bent ingelogd, kun je naar het url: https://app.hellodialog.com/import gaan en de csv optie kiezen om het geëxporteerde bestand te importeren.
Deze pagina is ook te bereiken door naar het contact overzicht te gaan via het verticale menu en vervolgens in het horizontale menu op "Importeren" te klikken.

### Order Synchronisatie
Hellodialog biedt ook ondersteuning om mail te sturen op basis van bepaalde order informatie. 
Bij elke order in de Magento 2 webshop wordt een deel van de orderinformatie naar Hellodialog verstuurd om geavanceerde email campagnes mogelijk te maken.
Je kunt deze informatie inzien via het url: https://app.hellodialog.com/ecommercelist/orders

### Licentie
Deze module is geschreven door [Zeo BV.](https://www.zeo.nl/) en wordt gratis uitgegeven aan de klanten van Hellodialog. Alle rechten worden aan Hellodialog voorbehouden.

---------------------------------------------
# English version


## Before you start

  - Make sure your Magento Shop is version 2.1.x. You can find the version number at the bottom right of the Magento 2 admin panel.
  - It's highly advised to first install any Magento module in a test environment first. Conflicts can occur with other installed modules.

## Installation

Follow the steps below to successfully install the Hellodialog module in your Magento 2.1.x shop:
1. Drag all the contents of the downloaded .zip file to the Magento 2.1.x installation root folder. You can recognize the installation root folder by the presence of the app, pub and var folders.
2. Open the terminal of your choice and SSH to the (test) server. Now, enter the following commands in the given order.

    ```sh
    $ php bin/magento module:enable Hellodialog_Base
    $ php bin/magento maintenance:enable
    $ php bin/magento setup:upgrade
    $ php bin/magento setup:di:compile
    $ php bin/magento setup:static-content:deploy # depending on the languages of your shop you might want run this command multiple times with the appropriate language codes. See the Magento 2 documentation if you are unsure about this.
    $ php bin/magento cache:clean
    $ php bin/magento cache:flush
    $ php bin/magento indexer:reindex
    $ php bin/magento maintenance:disable
    ```

3. Navigate to the admin panel of the shop and go to STORES > CONFIGURATION.
4. Click under the tab "Hellodialog" on the label "Advanced". Here you will find the fields needed to setup the connection between Hellodialog and your Magento 2 installation. The fields "Api Key", "Api Secret" and "Request Url".
    - Note that the "Request Url" field already contains a default value. You can leave this as is. This field only needs to be edited under special circumstances.
5. Open a new tab, navigate to https://app.hellodialog.com/account/login en log in.
6. This will open your Hellodialog Dashboard. On the left-hand side, you will find a vertical menu. click on the item "Settings"
7. Next, find the horizontal menu under the title "General Settings" and click on menu item "API".
8. In the case you didn't create an API key before, click on "Add key", else continue from step 11.
9. A dialog will open with a couple of fields, enter the following values:
    - Description: "Magento {Your Magento version, 1 of 2}"
    - Access: ALL ALL
    - webhook-url: {www.yourdomain.com}/hellodialog
    - IP-restriction: {If you are an advanced user, enter:} Restricted (secure) {else:} Unrestricted (insecure)
    - IP-address: {This field will only appear if you selected "secure" for IP-restriction} This field requires the IP-address of the server requests, to Hellodialog, will be coming from.
10. Click the "Save"-button. Your API-key has been created.
11. Copy the value (from the row of the API-key you just created) from the "Key" column and paste it in the "Api Key"-field in de Magento configuration.
12. In the same row you will find on the right-hand side, a question mark icon. Hover on this icon, a text balloon will appear, and copy the value from under the "secret:" label and paste it in the "Secret Key"-field in de Magento configuration.
13. Now the connection is setup. To make the sync function properly you can configure the module further under the general tab. Here you can map the fields you created in Hellodialog to the fields used by Magento.
    - IMPORTANT: It's likely that the default values don't yet correspond to the fields of your Hellodialog account, since these are language depended. 
    You can find the correct values by going to the following url: https://app.hellodialog.com/contacts/fields or by clicking the menu item "Contacts" from the vertical menu of your Hellodialog dashboard followed by clicking the item "Fields" in the horizontal menu. 

## Extra information
### Sync Contacts
The Hellodialog Magento 2 module makes it possible to get existing contacts from Hellodialog to Magento 2 using the "Sync Contacts" button.
During this process there will only be subscribers added and updated in your Magento 2 installation, which means we won't remove existing subscribers already present. This synchronization is based on the email address and status of the subscription.

### Export Contacts
Below the so called "Sync Contacts" button, there is also an "Export Contacts" button. 
This button has the function to export all your Magento 2 newsletter subscribers to import them manually into Hellodialog after.
If logged into the Hellodialog environment, you can browse to the url: https://app.hellodialog.com/import and chose the csv option to import the exported file.
You can also reach this page by clicking the "Contacts" item in the vertical menu and "Import" in the appearing horizontal menu.

### Order Synchronization
Hellodialog also offers support to send emails based on certain order information. 
With every order in the Magento 2 webshop, a part of the order information is send to Hellodialog to make advanced email campaigns possible.
You can find an overview of all orders present in Hellodialog via the url: https://app.hellodialog.com/ecommercelist/orders

### License
This module was written by [Zeo BV.](https://www.zeo.nl/) and is distributed for free to clients of Hellodialog. All rights are reserved by Hellodialog.

