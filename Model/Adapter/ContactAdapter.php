<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model\Adapter;

use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Adapter\Mapping\ContactFieldMap;

class ContactAdapter {

	protected $subscriber;
	protected $contactFieldMap;

	/**
	 * Initialize dependencies.
	 *
	 * @param Subscriber      $subscriber
	 * @param ContactFieldMap $contactFieldMap
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
	 */
	public function __construct(
		Subscriber $subscriber,
		ContactFieldMap $contactFieldMap
	)
	{
		$this->subscriber = $subscriber;
		$this->contactFieldMap = $contactFieldMap;
	}

	public function mapSubscriberToHDContact($magentoSubscriber, $isUpdateAction = false)
	{
		$magentoSubscriber = $this->subscriber->load($magentoSubscriber->getId());

		$helloDialogContact = [];

		if($isUpdateAction)  $helloDialogContact[ $this->contactFieldMap->idField() ] = $magentoSubscriber->getHellodialogId();
		$helloDialogContact[$this->contactFieldMap->emailField()] = $magentoSubscriber->getSubscriberEmail();
		$helloDialogContact[$this->contactFieldMap->stateField()] = $this->getHellodialogState($magentoSubscriber);
		$helloDialogContact[ $this->contactFieldMap->firstnameField() ] = ($magentoSubscriber->getFirstname()) ? $magentoSubscriber->getFirstname() : null;
		$helloDialogContact[ $this->contactFieldMap->lastnameField() ] = ($magentoSubscriber->getLastname()) ? $magentoSubscriber->getLastname() : null;

		return $helloDialogContact;
	}

	public function getFieldMapping()
	{
		return $this->contactFieldMap;
	}

	public function getHellodialogState(Subscriber $subscriber)
	{
		switch ($subscriber->getStatus()) {
			case $subscriber::STATUS_SUBSCRIBED:
				$state = "Contact";
				break;
			case $subscriber::STATUS_UNSUBSCRIBED:
				$state = "Unsubscriber";
				break;
			case $subscriber::STATUS_NOT_ACTIVE:
				$state = "Optin";
				break;
			default:
				$state = "Optin";
				break;
		}

		return $state;
	}
}