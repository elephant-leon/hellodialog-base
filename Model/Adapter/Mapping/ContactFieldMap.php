<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model\Adapter\Mapping;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ContactFieldMap {

	protected $scopeConfig;

	protected $apiKey;
	protected $apiSecret;
	protected $apiUrl;

	protected $idField;
	protected $emailField;
	protected $firstnameField;
	protected $lastnameField;
	protected $stateField;

	/**
	 * Initialize dependencies.
	 *
	 * @param ScopeConfigInterface $scopeConfig
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
	 */
	public function __construct(
		ScopeConfigInterface $scopeConfig
	)
	{
		$this->scopeConfig = $scopeConfig;

		$this->idField = 'id';
		$this->emailField = $this->scopeConfig->getValue('hellodialog_general/field_mapping/email', ScopeInterface::SCOPE_STORE);
		$this->firstnameField = $this->scopeConfig->getValue('hellodialog_general/field_mapping/firstname', ScopeInterface::SCOPE_STORE);
		$this->lastnameField = $this->scopeConfig->getValue('hellodialog_general/field_mapping/lastname', ScopeInterface::SCOPE_STORE);
		$this->stateField = "_state";
	}

	public function idField()
	{
		return strtolower( $this->idField );
	}

	public function emailField()
	{
		return strtolower( $this->emailField );
	}

	public function firstnameField()
	{
		return strtolower( $this->firstnameField );
	}

	public function lastnameField()
	{
		return strtolower( $this->lastnameField );
	}

	public function stateField()
	{
		return strtolower( $this->stateField );
	}

	public function _toArray( $includeHdId = false )
	{
		$fieldMapping = [];

		if( $includeHdId ) $fieldMapping[] = $this->idField();
		$fieldMapping[] = $this->emailField();
		$fieldMapping[] = $this->stateField();
		$fieldMapping[] = $this->firstnameField();
		$fieldMapping[] = $this->lastnameField();

		return $fieldMapping;
	}
}