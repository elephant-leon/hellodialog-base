<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model\Adapter;

use Hellodialog\Base\Model\Subscriber;

class SubscriberAdapter {

	protected $subscriber;

	/**
	 * Initialize dependencies.
	 *
	 * @param Subscriber $subscriber
	 */
	public function __construct(
		Subscriber $subscriber
	)
	{
		$this->subscriber = $subscriber;
	}

	/**
	 * @param \stdClass $contact
	 *
	 * @return Subscriber
	 */
	public function getSubscriberFromContact($contact)
	{
		$this->subscriber->setSubscriberEmail($contact->email);

		return $this->subscriber;
	}

	/**
	 * @param string $hdState
	 *
	 * @return integer
	 */
	public function getSubscriberState($hdState)
	{
		switch ($hdState) {
			case "Contact":
				$state = $this->subscriber::STATUS_SUBSCRIBED;
				break;
			case "Unsubscriber":
				$state =  $this->subscriber::STATUS_UNSUBSCRIBED;
				break;
			case "Optin":
				$state = $this->subscriber::STATUS_NOT_ACTIVE;
				break;
			default:
				$state = $this->subscriber::STATUS_NOT_ACTIVE;
				break;
		}

		return $state;
	}
}