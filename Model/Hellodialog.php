<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model;

use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\Customer as CustomerModel;
use Magento\Framework\App\Request\Http;

use Hellodialog\Base\Helper\Connector\HellodialogConnector;
use Hellodialog\Base\Helper\Connector\HellodialogConnectorFactory;
use Hellodialog\Base\Model\Adapter\ContactAdapter;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Sales\Model\OrderRepository;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Event\Manager as EventManager;

class Hellodialog {
    /** @var Logger  */
	protected $logger;
	protected $customerSession;
	protected $request;
	protected $hellodialogConnectorFactory;
	protected $contactAdapter;
	protected $config;
	protected $customerModel;
	protected $subscriber;
    protected $categoryRepository;
    protected $eventManager;

    /** @var OrderRepository  */
    protected $orderRepository;
    /** @var searchCriteriaInterface  */
    protected $searchCriteriaInterface;
    /** @var FilterBuilder  */
    protected $filterBuilder;
    /** @var FilterGroup  */
    protected $filterGroup;

	protected $apiUrl;
	protected $apiKey;

    /**
     * Hellodialog constructor.
     *
     * @param Context $context
     * @param Config $config
     * @param CustomerModel $customerModel
     * @param CustomerSession $customerSession
     * @param ContactAdapter $contactAdapter
     * @param HellodialogConnectorFactory $hellodialogConnectorFactory
     * @param Subscriber $subscriber
     * @param Logger $logger
     * @param Http $frameworkRequest
     * @param CategoryRepository $categoryRepository
     * @param OrderRepository $orderRepository
     * @param SearchCriteriaInterface $searchCriteriaInterface
     * @param FilterBuilder $filterBuilder
     * @param FilterGroup $filterGroup
     */
	public function __construct(
		Context $context,
		Config $config,
		CustomerModel $customerModel,
		CustomerSession $customerSession,
		ContactAdapter $contactAdapter,
        HellodialogConnectorFactory $hellodialogConnectorFactory,
		Subscriber $subscriber,
		Logger $logger,
		Http $frameworkRequest,
        CategoryRepository $categoryRepository,
        OrderRepository $orderRepository,
        SearchCriteriaInterface $searchCriteriaInterface,
        FilterBuilder $filterBuilder,
        FilterGroup $filterGroup,
        EventManager $eventManager
    )
	{
		$this->logger = $logger;
		$this->customerSession = $customerSession;
		$this->customerModel = $customerModel;
		$this->request = $frameworkRequest;

		$this->config = $config;
		$this->contactAdapter = $contactAdapter;
		$this->hellodialogConnectorFactory = $hellodialogConnectorFactory;
		$this->subscriber = $subscriber;
        $this->categoryRepository = $categoryRepository;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaInterface = $searchCriteriaInterface;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroup = $filterGroup;
        $this->eventManager = $eventManager;

        $this->apiKey = $this->config->getApiKey();
	}

	public function processSubscriberForHD( Subscriber $subscriber, $requestBody = [] )
	{
		$this->subscriber = $subscriber;

		$this->setSubscriberStatus();
		$requestBody = $this->prepareContactRequestBody($requestBody);

		if($result = $this->subscriberIsHellodialogContact( $subscriber->getHellodialogId(), $subscriber->getEmail() ) ) {
		    $hdId = $result;
			$this->update($hdId, $requestBody, 'contacts');

            $subscriber->setHellodialogId($hdId);
        } else {
			$response = $this->create($requestBody, 'contacts');

			if(isset($response->result->data->id)) {
				$hdId = $response->result->data->id;
				$subscriber->setHellodialogId($hdId);
            } else {
				$this->logger->log( __('Unexpected response received, failed to set hellodialog_id. Error: %1', [json_encode($response)]), Logger::ERROR );
			}
		}

		return $subscriber;
	}

    public function processOrdersForHD( $subscriber )
    {
        $this->logger->log( __('Starting to process orders for subscriber with HD ID %1', [$subscriber->getHellodialogId()]) );
        if( $subscriber->getHellodialogId() ) {
            $result = $this->getOrdersForSubscriber( $subscriber );
            $this->logger->log( __('Number of orders found: %1', [count($result->getItems())]) );

            foreach( $result->getItems() as $item ) {
                $this->processOrderForHD( $item, $subscriber );
            }
        }
	}

    /**
     * @param Subscriber $subscriber
     *
     * @return \Magento\Sales\Api\Data\OrderInterface[]|\Magento\Sales\Api\Data\OrderSearchResultInterface
     */
    public function getOrdersForSubscriber( $subscriber )
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('customer_email')
                ->setConditionType('eq')
                ->setValue($subscriber->getEmail())
                ->create(),
        ]);

        $this->searchCriteriaInterface->setFilterGroups([$this->filterGroup]);

        return $this->orderRepository->getList( $this->searchCriteriaInterface );
	}

    /**
     * @param $order
     * @param Subscriber $subscriber
     *
     * @return mixed|null
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function processOrderForHD( $order, $subscriber = null )
	{
        if( is_null($subscriber) ) {
            $subscriber = $this->subscriber->loadByEmail( $order->getCustomerEmail() );
            $hdId = $subscriber->getHellodialogId();
        } else {
            try {
                $subscriber = $this->subscriber->load( $subscriber->getId() );
            } catch ( \Magento\Framework\Exception\LocalizedException $e ) {
                # can't find subscriber
                $subscriber = null;
            }
            $hdId = $subscriber->getHellodialogId();
        }

        $this->logger->log( __('Loaded subscriber with Hello Dialog id %1', [$hdId]));

		if( $subscriber ) {
            if( $hdId ) {
                $this->logger->log( __('Preparing order for subscriber with HD ID: %1', [$hdId]));

                $requestBody = $this->prepareOrderRequestBody( $order, $subscriber );

                $response = $this->create($requestBody, 'orders');

                if( !isset($response->result) || $response->result->status !== "OK" ) {
                    $this->logger->log( __('Failed to export order to Hellodialog. Response content: %1', [json_encode($response)]), Logger::ERROR );
                } else {
                    $this->logger->log( __('Exported orders successfully. Response content: %1', [json_encode($response)]) );
                }
            } else {
                $response = null;
                $this->logger->log( __('Subscriber does not have an Hellodialog Id.'), Logger::ERROR );
            }
        } else {
            $response = null;
            $this->logger->log( __('No subscriber found for order.'), Logger::ERROR );
		}

		return $response;
	}

	public function create( $requestBody, $endpoint )
	{
        $helloDialogConnector = $this->initEndpoint( $endpoint );

        $this->logger->log( __('Post request to endpoint %1 with request body %2.', [$helloDialogConnector->getEndpoint(), json_encode($requestBody)]), Logger::ERROR );

        $response = $helloDialogConnector->post($requestBody);

		$helloDialogDecodedResponse = json_decode( $response->getContent() );

		return $helloDialogDecodedResponse;
	}

	public function update( $hdId, $requestBody, $endpoint )
	{
        $helloDialogConnector = $this->initEndpoint( $endpoint );

        $response = $helloDialogConnector->put($hdId, $requestBody);

		$helloDialogDecodedResponse = json_decode( $response->getContent() );

		return $helloDialogDecodedResponse;
	}

	protected function prepareContactRequestBody( $requestBody ) {
		$contactFieldMapping = $this->contactAdapter->getFieldMapping();

		$requestBody[ $contactFieldMapping->emailField() ] = $this->subscriber->getEmail();
		$requestBody[ $contactFieldMapping->stateField() ] = $this->contactAdapter->getHellodialogState( $this->subscriber );

		// When a magento 2 profile is saved the values with keys "firstname" and "lastname" become available in the request object
		if( !$this->request->getPostValue('firstname') && $this->customerSession->isLoggedIn()) {
			$customer = $this->customerSession->getCustomer();
			$requestBody[ $contactFieldMapping->firstnameField() ] = $customer->getFirstname(); # is magic function
			$requestBody[ $contactFieldMapping->lastnameField() ] = $customer->getLastname(); # is magic function
		} else if($this->request->getPostValue('firstname') && $this->request->getPostValue('lastname')) {
			$requestBody[ $contactFieldMapping->firstnameField() ] 	= $this->request->getPostValue('firstname');
			$requestBody[ $contactFieldMapping->lastnameField() ] 	= $this->request->getPostValue('lastname');
		}

		return $requestBody;
	}

	public function prepareOrderRequestBody( Order $order, Subscriber $subscriber ) {
		$requestBody = [];

		if( $hdId = $subscriber->getHellodialogId() ) {
			$order_id = $order->getIncrementId();

			$requestBody['contact'] = $hdId; // Required
			$requestBody['order_number'] = $order_id; // Required
			$requestBody['created_on'] = $order->getCreatedAt() ? strtotime($order->getCreatedAt()) : time(); // Required
			$requestBody['price'] = $order->getGrandTotal(); // required

			if( $order->getPayment() ) {
				$amountPaid = $order->getPayment()->getData('amount_paid');
				$amountRefunded = $order->getPayment()->getData('amount_refunded');
				$amountCancelled = $order->getPayment()->getData('amount_canceled');
				$requestBody['payment_method'] = $order->getPayment()->getMethodInstance()->getTitle();
				$requestBody['payment_status'] = !is_null($amountCancelled) ? 'CANCELLED' : !is_null($amountRefunded) ? 'REFUND' : is_null($amountPaid) ? 'PENDING' : 'PAID';
			}

			$requestBody['discount'] = $order->getDiscountAmount();

			if( $order->getBillingAddress() ) {
				$requestBody['zip_code'] = $order->getBillingAddress()->getPostcode();
				$requestBody['country'] = $order->getBillingAddress()->getCountryId();
			}

			$requestBody['coupon'] = $order->getCouponCode();

			/** @var Order\Item $orderItem */
			foreach ($order->getAllItems() as $orderItem) {
                /** @var Product $product */
                $product = $orderItem->getProduct();

                $products = [];
                if( $product->getTypeId() === "simple" ) {
                    $products['order'] = $order_id;
                    $products['product_code'] = $product->getSku();
                    $products['name'] = $product->getName();
                    $products['quantity'] = $orderItem->getQtyOrdered();
                    $products['discount'] = $orderItem->getDiscountAmount();
                    $products['price'] = $product->getFinalPrice();

                    if( $product->getCategory() ) {
                        $products['category'] = $product->getCategory()->getName();
                    }

                    $requestBody['products'][] = $products;
                }
			}
		} else {
		    $this->logger->log( __('Failed to prepare order request body. Subscriber doesn\'t have an hellodialog id.'), Logger::ERROR );

			return $requestBody;
		}

		return $requestBody;
	}

    protected function getCategoriesOfProduct($product) {
        $categories = [];

        foreach ($product->getCategoryCollection() as $category) {
            $category = $this->categoryRepository->get($category->getId());

            $categoriePath = '';
            foreach ($category->getParentCategories($category) as $parentCategory) {
                $categoriePath .= '/' . $parentCategory->getName();
            }

            if(!empty($categoriePath)){
                $categories[] .= $categoriePath;
            }
        }

        return $categories;
    }

	private function setSubscriberStatus()
	{
		if($this->request->getPostValue('is_subscribed') && $this->subscriber->getStatus() === 3) {
			$this->subscriber->setStatus(Subscriber::STATUS_SUBSCRIBED);
		} elseif (is_null($this->request->getPostValue('is_subscribed')) && $this->subscriber->getStatus() === 1 && !$this->customerCredentialsArePosted()){
			$this->subscriber->setStatus(Subscriber::STATUS_UNSUBSCRIBED);
		}
	}

	public function subscriberIsHellodialogContact($hdId, $email)
	{
		if($hdId = $this->verifyHdContactById($hdId)) {
		    return $hdId;
		} else {
			return $this->verifyHdContactByEmail($email);
		}
	}

	public function verifyHdContactById($hdId)
	{
	    $helloDialogConnector = $this->initEndpoint('contacts');
		$response = $helloDialogConnector->get($hdId);
		$helloDialogDecodedResponse = json_decode( $response->getContent() );

		if( isset($helloDialogDecodedResponse->id) ) { // if id is set the subscriber is present
			return $helloDialogDecodedResponse->id;
		} else { /** if not, the subscriber is not present and they require a search query @see verifyHdContactByEmail() **/
			return null;
		}
	}

	public function verifyHdContactByEmail($email)
	{
		$emailField = $this->contactAdapter->getFieldMapping()->emailField();
		$options = [
			'condition' => [
				$emailField => 'equals',
			],
			'values'=> [
				$emailField => $email,
			],
		];

        $helloDialogConnector = $this->initEndpoint('contacts');
        $response = $helloDialogConnector->get(null, $options);
		$helloDialogDecodedResponse = json_decode( $response->getContent() );

		if(isset($helloDialogDecodedResponse->{0})) { // email has to be unique so we expect only one object back
			return $helloDialogDecodedResponse->{0}->id;
		} else { // if 0 is not set, no results were returned and we return false
			return null;
		}
	}

    protected function initEndpoint( $endpoint )
    {
        $this->logger->log( __('Init endpoint %1', [$endpoint]), Logger::ERROR );

        /** @var HellodialogConnector $helloDialogConnector */
        $helloDialogConnector = $this->hellodialogConnectorFactory->create();

        $apiUrl = $this->config->getApiUrl( $endpoint );
        $helloDialogConnector->init( $this->apiKey, $apiUrl );

        return $helloDialogConnector;
	}

	private function customerCredentialsArePosted()
	{
		return ( !is_null($this->request->getPostValue('firstname')) || !is_null($this->request->getPostValue('lastname')) || !is_null($this->request->getPostValue('email')) ) ;
	}
}