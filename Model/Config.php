<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Information as StoreInformation;
use Magento\Store\Model\Store;

class Config {

	const HELLODIALOG_DEBUG_MODE_PATH = 'hellodialog_advanced/api_config/debug_mode';
	const HELLODIALOG_API_KEY_PATH = 'hellodialog_advanced/api_config/api_key';
	const HELLODIALOG_API_SECRET_PATH = 'hellodialog_advanced/api_config/api_secret';
	const HELLODIALOG_API_URL_PATH = 'hellodialog_advanced/api_config/api_url';

	const STORE_EMAIL = 'trans_email/ident_general/email';

	/** @var ScopeConfigInterface  */
	protected $scopeConfig;
	/** @var  Store */
	protected $store;
	/** @var \Magento\Framework\DataObject  */
	protected $storeInfo;

	/**
	 * Config constructor.
	 *
	 * @param ScopeConfigInterface $scopeConfig
	 * @param StoreInformation     $storeInfo
	 * @param Store                $store
	 */
	public function __construct(
		ScopeConfigInterface $scopeConfig,
		StoreInformation $storeInfo,
		Store $store
	)
	{
		$this->scopeConfig = $scopeConfig;
		$this->store = $store;
		$this->storeInfo = $storeInfo->getStoreInformationObject( $store );
	}

	public function getDebugMode()
	{
		return $this->getValue( self::HELLODIALOG_DEBUG_MODE_PATH );
	}

	public function getApiKey()
	{
		return $this->getRequiredValue( self::HELLODIALOG_API_KEY_PATH );
	}

	public function getApiSecret()
	{
		return $this->getRequiredValue( self::HELLODIALOG_API_SECRET_PATH );
	}

	public function getApiUrl( $endpoint )
	{
		$apiBaseUrl = $this->getApiBaseUrl();

		$apiUrl = ( substr($apiBaseUrl, -1) == '/' ) ? $apiBaseUrl.$endpoint : "$apiBaseUrl/$endpoint";

		return $apiUrl;
	}

	public function getApiBaseUrl()
	{
		return $this->getRequiredValue( self::HELLODIALOG_API_URL_PATH );
	}

	public function getStoreName()
	{
		return $this->storeInfo->getName();
	}

	public function getStoreHours()
	{
		return $this->storeInfo->getHours();
	}

	public function getStorePhone()
	{
		return $this->storeInfo->getPhone();
	}

	public function getStoreEmail()
	{
		return $this->getValue( self::STORE_EMAIL );
	}

	public function getStoreUrl()
	{
		return $this->store->getBaseUrl();
	}

	public function getValue( $systemFieldPath )
	{
		return $this->scopeConfig->getValue( $systemFieldPath, ScopeInterface::SCOPE_STORE );
	}

	public function getRequiredValue( $systemFieldPath )
	{
		$value = $this->scopeConfig->getValue( $systemFieldPath, ScopeInterface::SCOPE_STORE );

		if( is_null($value) ) {
			throw new \Magento\Framework\Exception\LocalizedException( __($systemFieldPath . ' is required.') );
		}

		return $value;
	}
}