<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model;

use Hellodialog\Base\Helper\Connector\HellodialogConnector;
use Hellodialog\Base\Model\Adapter\SubscriberAdapter;
use Hellodialog\Base\Model\Config;
use Magento\Ui\Component\Form\Field;

/**
 * Subscriber model
 *
 * @method \Magento\Newsletter\Model\ResourceModel\Subscriber _getResource()
 * @method \Magento\Newsletter\Model\ResourceModel\Subscriber getResource()
 *
**/
class Subscriber extends \Magento\Newsletter\Model\Subscriber {

	protected $config;
	protected $hdConnector;
	protected $subscriberAdapter;

	/**
	 * Initialize dependencies.
	 *
	 * @param HellodialogConnector                                    $hdConnector
	 * @param Config                                         		  $config
	 * @param \Magento\Framework\Model\Context                        $context
	 * @param \Magento\Framework\Registry                             $registry
	 * @param \Magento\Newsletter\Helper\Data                         $newsletterData
	 * @param \Magento\Framework\Mail\Template\TransportBuilder       $transportBuilder
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface      $scopeConfig
	 * @param \Magento\Store\Model\StoreManagerInterface              $storeManager
	 * @param \Magento\Customer\Model\Session                         $customerSession
	 * @param \Magento\Customer\Api\CustomerRepositoryInterface       $customerRepository
	 * @param \Magento\Customer\Api\AccountManagementInterface        $customerAccountManagement
	 * @param \Magento\Framework\Translate\Inline\StateInterface      $inlineTranslation
	 * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
	 * @param \Magento\Framework\Data\Collection\AbstractDb           $resourceCollection
	 * @param array                                                   $data
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
	 */
	public function __construct(
		HellodialogConnector $hdConnector,
		Config $config,
		\Magento\Framework\Model\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Newsletter\Helper\Data $newsletterData,
		\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
		\Magento\Customer\Api\AccountManagementInterface $customerAccountManagement,
		\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
		\Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
		\Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
		array $data = []
	) {
        $this->subscriberAdapter = new SubscriberAdapter($this);
		$this->hdConnector = $hdConnector;

		$apiKey = $config->getApiKey();
		$apiUrl = $config->getApiUrl('contacts' );
		$this->hdConnector->init($apiKey, $apiUrl);

		parent::__construct($context,
			$registry,
			$newsletterData,
			$scopeConfig,
			$transportBuilder,
			$storeManager,
			$customerSession,
			$customerRepository,
			$customerAccountManagement,
			$inlineTranslation,
			$resource,
			$resourceCollection
		);
	}

    /**
     * @param int $modelId
     * @param null $field
     *
     * @return $this|\Magento\Newsletter\Model\Subscriber
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
	public function load($modelId, $field = null)
	{
		$this->_beforeLoad($modelId, $field);
		$this->_getResource()->load($this, $modelId, $field);
		$this->_afterLoad();
		$this->setOrigData();
		$this->_hasDataChanges = false;
		return $this;
	}

	/**
	 * Load subscriber from DB by email
	 *
	 * @param string $hdId
	 * @return $this
	 */
	public function loadByHellodialogId( $hdId )
	{
		$this->addData( $this->getResource()->loadByHellodialogId( $hdId ) );

		return $this;
	}

	public function update()
	{
		$response = $this->hdConnector->get( $this->getHellodialogId() );

		$response = $response->getContent();

		$result = [
			"result" => [
				"status" 	=>	"ERROR",
				"message"	=> 	"An error occurred",
				"code" 		=> 	403
			]
		];
		if( isset($response->email) ) {
			$result["fields_updated"]["email"]["from"] = $this->getSubscriberEmail();
			$this->setSubscriberEmail( $response->email );
			$result["fields_updated"]["email"]["to"] = $this->getSubscriberEmail();
		}
		if( isset($response->_state) ) {
			$result["fields_updated"]["status"]["from"] = $this->getStatus();

			$subscriberState = $this->subscriberAdapter->getSubscriberState( $response->_state );
			$this->setSubscriberStatus( $subscriberState );

			$result["fields_updated"]["status"]["to"] = $this->getStatus();

		}
		if($this->save()->getId()) {
			$result["result"]["status"] 	= "OK";
			$result["result"]["code"] 		= "200";
			$result["result"]["message"] 	= "Save completed";
		} else {
			$result["result"]["message"] 	= "Save failed";
		}

		return $result;
	}
}