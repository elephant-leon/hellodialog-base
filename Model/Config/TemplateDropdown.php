<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model\Config;

use Hellodialog\Base\Model\Config;
use Hellodialog\Base\Helper\Connector\HellodialogConnector;

class TemplateDropdown implements \Magento\Framework\Option\ArrayInterface
{
	/** @var Config  */
	protected $config;
	/** @var  HellodialogConnector */
	protected $hellodialogConnector;

	/** @var array  */
	protected $templateList;

	/**
	 * TemplateDropdown constructor.
	 *
	 * @param Config               $config
	 * @param HellodialogConnector $hellodialogConnector
	 */
	public function __construct(
		Config $config,
		HellodialogConnector $hellodialogConnector
	)
	{
		$this->config = $config;
		$this->hellodialogConnector = $hellodialogConnector;

		$this->templateList = $this->retrieveTemplateList();
	}

	/**
	 * Options getter
	 *
	 * @return array
	 */
	public function toOptionArray()
	{
		$templateList = [];

		foreach ($this->templateList as $template) {
			if( isset($template->id) && isset($template->name) ) {
				$templateList[] = [
					'value' => $template->id,
					'label' => $template->name . " (ID: $template->id)",
				];
			}
		}

		return $templateList;
	}

	/**
	 * Get options in "key-value" format
	 *
	 * @return array
	 */
	public function toArray()
	{
		$templateList = [];

		foreach ($this->templateList as $template) {
			if( isset($template->id) && isset($template->name) ) {
				$templateList[ $template->id ] = $template->name;
			}
		}

		return $templateList;
	}

	protected function retrieveTemplateList()
	{
		$apiKey = $this->config->getApiKey();
		$apiUrl = $this->config->getApiUrl('newsletters');
		$this->hellodialogConnector->init( $apiKey, $apiUrl );

		$page = 1;
		$contentRetrieved = true;
		$templateList = [];

		while( $contentRetrieved ) {
			$response = $this->hellodialogConnector->get(null, ['page' => $page]);
			$content = (array)json_decode( $response->getContent() );

			$contentRetrieved = !empty( $content );

			foreach ($content as $item) {
				$templateList[] = $item;
			}

			$page++;
		}

		return $templateList;
	}
}
