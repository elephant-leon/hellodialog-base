<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model;

use Magento\Framework\Logger\Handler\Base;
use Magento\Framework\Filesystem\DriverInterface;
use Monolog\Logger as MonoLogger;

class Logger extends Base {

	/** Detailed debug information */
	const DEBUG = MonoLogger::DEBUG;

	/**Interesting events */
	const INFO = MonoLogger::INFO;

	/** Uncommon events */
	const NOTICE = MonoLogger::NOTICE;

	/** Exceptional occurrences that are not errors */
	const WARNING = MonoLogger::WARNING;

	/** Runtime errors */
	const ERROR = MonoLogger::ERROR;

	/**Critical conditions */
	const CRITICAL = MonoLogger::CRITICAL;

	/** Action must be taken immediately */
	const ALERT = MonoLogger::ALERT;

	/** Urgent alert.*/
	const EMERGENCY = MonoLogger::EMERGENCY;

	/** @var string  */
	protected $fileName = '/var/log/hellodialog_debug.log';

	/** @var int  */
	protected $loggerType = MonoLogger::INFO;

	/** @var Config */
	protected $config;

	/**
	 * Logger constructor.
	 *
	 * @param \Hellodialog\Base\Model\Config $config
	 * @param DriverInterface                $filesystem
	 * @param null                           $filePath
	 */
	public function __construct(
		Config $config,
		DriverInterface $filesystem,
		$filePath = null
	) {
		$this->config = $config;
		parent::__construct( $filesystem, $filePath );
	}

	public function log( $message, $level = self::DEBUG )
	{
		$this->loggerType = $level;
		if( $this->config->getDebugMode() ) {
			$record['formatted'] = $message . PHP_EOL;
			$this->write($record);
		}
	}
}