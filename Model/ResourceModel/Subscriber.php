<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Model\ResourceModel;

class Subscriber extends \Magento\Newsletter\Model\ResourceModel\Subscriber {

	/**
	 * Construct
	 *
	 * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
	 * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
	 * @param \Magento\Framework\Math\Random $mathRandom
	 * @param string $connectionName
	 */
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Magento\Framework\Math\Random $mathRandom,
		$connectionName = null
	) {
		parent::__construct($context, $date, $mathRandom, $connectionName);
	}

	/**
	 * Load subscriber from DB by email
	 *
	 * @param string $hdId
	 * @return array
	 */
	public function loadByHellodialogId($hdId)
	{
		$select = $this->connection->select()->from($this->getMainTable())->where('hellodialog_id=:hellodialog_id');

		$result = $this->connection->fetchRow($select, ['hellodialog_id' => $hdId]);

		if (!$result) {
			return [];
		}

		return $result;
	}
}