<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Controller\Contact;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;

use Hellodialog\Base\Model\Hellodialog;
use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Config;

class Update extends Action {

	protected $resultJsonFactory;
	protected $scopeConfig;
	protected $hellodialog;
	protected $subscriber;
	protected $config;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Action\Context              $context
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeInterface
	 * @param \Magento\Framework\Controller\Result\JsonFactory   $resultJsonFactory
	 * @param \Hellodialog\Base\Model\Hellodialog                $hellodialog
	 * @param \Hellodialog\Base\Model\Subscriber                 $subscriber
	 * @param \Hellodialog\Base\Model\Config            		 $config
	 */
	public function __construct(
		Context $context,
		ScopeConfigInterface $scopeInterface,
		JsonFactory $resultJsonFactory,
		Hellodialog $hellodialog,
		Subscriber $subscriber,
		Config $config
	)
	{
		$this->resultJsonFactory = $resultJsonFactory;
		$this->scopeConfig = $scopeInterface;
		$this->hellodialog = $hellodialog;
		$this->subscriber = $subscriber;
		$this->config = $config;


		parent::__construct($context);
	}

	public function execute()
	{
		$post = $this->getRequest()->getPost();
		$result = $this->resultJsonFactory->create();
		$subscriber = $this->subscriber->loadByHellodialogId( $post->id );

		if($this->requestIsValid($post->hash) && !is_null( $subscriber->getId() ) ) {
			$actionResult = $subscriber->update();
			$result->setData($actionResult);
			$result->setStatusHeader(200);
		} else {
			$actionResult = [
				"result" => [
					"status" 	=>	"ERROR",
					"message"	=> 	"Either the provided key is invalid or the contact you are trying to update doesn't exists",
					"code" 		=> 	403
				]
			];
			$result->setData($actionResult);

			$result->setStatusHeader(403);
		}

		return $result;
	}

	private function requestIsValid($givenHash) {
		$apiHash =  md5($this->config->getApiSecret() . $this->config->getApiKey() );

		return ($apiHash === $givenHash);
	}
}