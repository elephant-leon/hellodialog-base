<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Controller\Product;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;

use Hellodialog\Base\Model\Config;

class Get extends Action {

	protected $scopeConfig;
	protected $product;
	protected $categoryRepository;
	protected $config;
	protected $result;
	protected $imageHelper;
	protected $priceCurrency;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Action\Context              $context
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeInterface
	 * @param \Magento\Framework\Controller\Result\JsonFactory   $resultJsonFactory
	 * @param \Hellodialog\Base\Model\Config            		 $config
	 * @param \Magento\Catalog\Model\Product                     $product
	 * @param \Magento\Catalog\Model\categoryRepository          $categoryRepository
	 */
	public function __construct(
		Context $context,
		ScopeConfigInterface $scopeInterface,
		JsonFactory $resultJsonFactory,
		Config $config,
		Product $product,
		CategoryRepository $categoryRepository,
		ImageHelper $imageHelper,
        PriceHelper $priceCurrency
	)
	{
		$this->scopeConfig = $scopeInterface;
		$this->product = $product;
		$this->categoryRepository = $categoryRepository;
		$this->result = $resultJsonFactory->create();
		$this->config = $config;
		$this->imageHelper = $imageHelper;
		$this->priceCurrency = $priceCurrency;

		parent::__construct($context);
	}

	public function execute()
	{
		$sku = $this->getRequest()->getParam('sku');
		$hash = $this->getRequest()->getParam('hash');

		if( $this->requestIsValid($hash) && !is_null( $sku ) ) {
			$product = $this->product->loadByAttribute( 'sku', $sku );
			if($product instanceof Product) {
				$this->setResultWithProduct( $product );
			} else {
				$this->setResultItemNotFound();
			}
		} else {
			$this->setResultAccessDenied();
		}

		return $this->result;
	}

	/** Allows Hellodialog to fetch product information for use in the creation of newsletters
	 * 	@param Product $product
	 */
	protected function setResultWithProduct(Product $product)
	{
        $resultData = [
            "product_id"            => $product->getSku(),
            "product_sku"           => $product->getSku(),
            "product_title"         => $product->getName(),
            "product_image"         => $this->imageHelper->init($product, 'product_base_image')->getUrl(),//$this->getImageUrlsOfProduct($product),
            "product_price"         => strip_tags($this->priceCurrency->currency($product->getFinalPrice())),
            "product_price_discount"=> strip_tags($this->priceCurrency->currency($product->getSpecialPrice())),
            "product_link"          => $product->getProductUrl(),
            "product_description"   => $product->getDescription()
		];

		$this->result->setData($resultData);
		$this->result->setStatusHeader(200);
	}

	protected function getCategoriesOfProduct($product) {
		$categories = [];

        foreach ($product->getCategoryCollection() as $category) {
            $category = $this->categoryRepository->get($category->getId());

            $categoriePath = '';
            foreach ($category->getParentCategories($category) as $parentCategory) {
                $categoriePath .= '/' . $parentCategory->getName();
            }

            if(!empty($categoriePath)){
                $categories[] = $categoriePath;
            }
        }

		return $categories;
	}

	protected function getImageUrlsOfProduct(Product $product)
	{
		$imageUrls = [
			'base' => $baseImage = $this->imageHelper->init($product, 'product_base_image')->getUrl(),
			'small_image' => $smallImage = $this->imageHelper->init($product, 'product_small_image')->getUrl(),
			'thumbnail' => $thumbnailImage = $this->imageHelper->init($product, 'product_thumbnail_image')->getUrl(),
		];

		return $imageUrls;
	}

	private function setResultItemNotFound()
	{
		$resultData = [
			"result" => [
				"status" 	=>	"ERROR",
				"message"	=> 	"The product you are trying to find doesn't exists.",
				"code" 		=> 	404
			]
		];

		$this->result->setData($resultData);

		$this->result->setStatusHeader(403);
	}

	private function setResultAccessDenied()
	{
		$resultData = [
			"result" => [
				"status" 	=>	"ERROR",
				"message"	=> 	"Either the provided key is invalid or not sku was given. Be sure to follow the format /module/controller/action/param/value/param/value.",
				"code" 		=> 	403
			]
		];
		$this->result->setData($resultData);

		$this->result->setStatusHeader(403);
	}

	private function requestIsValid($givenHash) {
		$apiHash =  md5($this->config->getApiSecret() . $this->config->getApiKey() );

		return ($apiHash === $givenHash);
	}
}