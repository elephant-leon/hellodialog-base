<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Request\Http;

use Hellodialog\Base\Model\Config;
use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Hellodialog;
use Hellodialog\Base\Helper\Connector\HellodialogConnector;

class Export extends \Magento\Backend\App\Action
{
	/** @var Http  */
	protected $request;
	/** @var Config  */
	protected $config;
	/** @var OrderRepository  */
	protected $orderRepository;
	/** @var CustomerRepository */
	protected $customerRepository;
	/** @var SearchCriteriaBuilder  */
	protected $searchCriteriaBuilder;
	/** @var Subscriber  */
	protected $subscriber;
	/** @var Hellodialog  */
	protected $hellodialog;
	/** @var HellodialogConnector  */
	protected $hellodialogConnector;

	/**
	 * Export constructor.
	 *
	 * @param Http                  $request
	 * @param Context               $context
	 * @param Config                $config
	 * @param OrderRepository       $orderRepository
	 * @param SearchCriteriaBuilder $searchCriteriaBuilder
	 * @param Subscriber            $subscriber
	 * @param Hellodialog           $hellodialog
	 * @param HellodialogConnector  $hellodialogConnector
	 */
	public function __construct(
		Http $request,
		Context $context,
		Config $config,
		OrderRepository $orderRepository,
		CustomerRepository $customerRepository,
		SearchCriteriaBuilder $searchCriteriaBuilder,
		Subscriber $subscriber,
		Hellodialog $hellodialog,
		HellodialogConnector $hellodialogConnector
	) {
		$this->request = $request;
		$this->config = $config;
		$this->orderRepository = $orderRepository;
		$this->customerRepository = $customerRepository;
		$this->searchCriteriaBuilder = $searchCriteriaBuilder;
		$this->subscriber = $subscriber;
		$this->hellodialog = $hellodialog;
		$this->hellodialogConnector = $hellodialogConnector;

		parent::__construct($context);
	}

	public function execute() {

		$fromDate = $this->request->getParam('from');

		if( $fromDate ) {
            $customers = $this->customerRepository->getList( $this->searchCriteriaBuilder->create() );
            $ordersRequestBody = [];
            /** @var Customer $customer */
            foreach ($customers->getItems() as $customer) {
                $subscriber = $this->subscriber->loadByCustomerId( $customer->getId() );
                if( !$subscriber->isSubscribed() ) continue;

                $fromDate = date('Y-m-d H:i:s', strtotime($fromDate));
                $criteria = $this->searchCriteriaBuilder
                                 ->addFilter('created_at', $fromDate, 'gteq')
                                 ->addFilter('customer_id', $customer->getId())
                                 ->create();
                $orders = $this->orderRepository->getList( $criteria );
                var_dump($orders->getTotalCount());exit;
                /** @var Order $order */
                foreach ($orders as $order) {
                    $orderDetails = $this->hellodialog->prepareOrderRequestBody( $order, $subscriber );
                    if( !empty($orderDetails) ) {
                        $ordersRequestBody[] = $orderDetails;
                    }
                }

                if( !empty($ordersRequestBody) ) {
                    $responseContent = $this->sendOrdersRequestBodyToHellodialog( $ordersRequestBody );

                    $this->handleResponse( $responseContent );
                } else {
                    $this->messageManager->addErrorMessage( __('No orders found from active subscribers from date %1.', [$fromDate]) );
                }
            }

		} else {
			$this->messageManager->addErrorMessage( __('From which date and time would you like to export the orders? Be sure to fill in the day, month, year, hour and minute values.') );

			return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
		}

		return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setUrl($this->_redirect->getRefererUrl());
	}

    protected function sendOrdersRequestBodyToHellodialog( $ordersRequestBody )
    {
        $apiUrl = $this->config->getApiUrl('orders' );
        $this->hellodialogConnector->init( $this->config->getApiKey(), $apiUrl );
        $response = $this->hellodialogConnector->post( $ordersRequestBody );

        $responseContent = $response->getContent() ? json_decode($response->getContent(), true) : null;

        return $responseContent;
	}

    protected function handleResponse( $responseContent )
    {
        if( isset($responseContent['result']) && isset($responseContent['result']['status']) ) {
            if( $responseContent['result']['status'] === 'ERROR' ) {
                $rejectedOrders = [];
                foreach ($responseContent['result']['data']['errors'] as $errorData) {
                    if( $errorData['code'] === 639 ) { // Order is a duplicate
                        $rejectedOrders[] = $errorData['orderData']['order_number'];
                    }
                }
                if( count($rejectedOrders) ) {
                    $this->messageManager->addErrorMessage( __('Orders rejected as duplicates: %1.', [implode(', ', $rejectedOrders)]) );
                } else {
                    $this->messageManager->addErrorMessage( __('An unexpected error occurred: %1.', [json_encode($responseContent['result']['data']['errors'])]) );
                }
            }

            if( count($responseContent['result']['data']['success_data']) ) {
                $acceptedOrders = [];
                foreach ($responseContent['result']['data']['success_data'] as $successData) {
                    $acceptedOrders[] = $successData['order_number'];
                }
                $this->messageManager->addSuccessMessage( __('Successfully exported orders: %1.', [implode(', ', $acceptedOrders)]) );
            }
        } else {
            $this->messageManager->addErrorMessage( __('An unexpected error occurred: %1.', [json_encode($responseContent)]) );
        }
	}
}