<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Controller\Adminhtml\Contact;

use Magento\Backend\App\Action\Context;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Collection as SubscriberCollection;

use Hellodialog\Base\Helper\Connector\HellodialogConnector;
use Hellodialog\Base\Model\Hellodialog;
use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Config;
use Hellodialog\Base\Model\Adapter\SubscriberAdapter;
use Hellodialog\Base\Model\Adapter\ContactAdapter;

class Export extends \Magento\Backend\App\Action
{
	protected $hdConnector;
	protected $helloDialog;
	protected $subscriber;
	protected $config;
	protected $subscriberAdapter;
	protected $contactAdapter;
	protected $subscriberCollection;

	/**
	 * Initialize dependencies.
	 *
	 * @param \Magento\Backend\App\Action\Context                           $context
	 * @param \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $subscriberCollection
	 * @param \Hellodialog\Base\Model\Hellodialog                           $helloDialog
	 * @param \Hellodialog\Base\Model\Adapter\SubscriberAdapter             $subscriberAdapter
	 * @param \Hellodialog\Base\Model\Adapter\ContactAdapter                $contactAdapter
	 * @param \Hellodialog\Base\Model\Subscriber                            $subscriber
	 * @param \Hellodialog\Base\Model\Config                       			$config
	 * @param \Hellodialog\Base\Helper\Connector\HellodialogConnector       $hdConnector
	 */
	public function __construct(
		Context $context,
		SubscriberCollection $subscriberCollection,
		Hellodialog $helloDialog,
		SubscriberAdapter $subscriberAdapter,
		ContactAdapter $contactAdapter,
		Subscriber $subscriber,
		Config $config,
		HellodialogConnector $hdConnector
	) {
		$this->helloDialog = $helloDialog;
		$this->subscriber = $subscriber;
		$this->config = $config;
		$this->subscriberAdapter = $subscriberAdapter;
		$this->contactAdapter = $contactAdapter;
		$this->subscriberCollection = $subscriberCollection;
		$this->hdConnector = $hdConnector;

		$apiKey = $this->config->getApiKey();
		$apiUrl = $this->config->getApiUrl( 'contacts' );

		$this->hdConnector->init( $apiKey, $apiUrl );

		parent::__construct($context);
	}

	public function execute() {
		$magentoSubscribers  = $this->getMagento2Subscribers();

		$headerRow = $this->contactAdapter->getFieldMapping()->_toArray();
		$contactData = $this->prepareSubscriberArrayForExport( $magentoSubscribers );

		$result = $this->exportContactsToCSV( $headerRow, $contactData );
	}

	private function exportContactsToCSV( $headerRow, $contactData , $delimiter = ',')
	{
		$filename = "hd-contacts-".time().".csv";
		$tmpFile = fopen( 'php://output', "w" );

		header( 'Content-Type: application/csv' );
		header( 'Content-Disposition: attachment; filename="'.$filename.'";' );

		fputcsv( $tmpFile, $headerRow, $delimiter );

		foreach ( $contactData as $contact ) {
			fputcsv( $tmpFile, $contact, $delimiter );
		}
	}

	private function prepareSubscriberArrayForExport($subscriberCollection)
	{
		$contactData = [];

		foreach ($subscriberCollection as $subscriber) {
			$contactData[] = $this->contactAdapter->mapSubscriberToHDContact($subscriber);
		}

		return $contactData;
	}

	private function getMagento2Subscribers()
	{
		$subscriberCollection = $this->subscriberCollection->showCustomerInfo()->getItems();

		return $subscriberCollection;
	}
}