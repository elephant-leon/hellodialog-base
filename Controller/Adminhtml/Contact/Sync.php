<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Controller\Adminhtml\Contact;

use Magento\Backend\App\Action\Context;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Collection as SubscriberCollection;

use Hellodialog\Base\Helper\Connector\HellodialogConnector;
use Hellodialog\Base\Model\Hellodialog;
use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Config;
use Hellodialog\Base\Model\Adapter\SubscriberAdapter;
use Hellodialog\Base\Model\Adapter\ContactAdapter;

class Sync extends \Magento\Backend\App\Action
{
	protected $hdConnector;
	protected $helloDialog;
	protected $subscriber;
	protected $config;
	protected $subscriberAdapter;
	protected $contactAdapter;
	protected $subscriberCollection;

	/**
	 * Initialize dependencies.
	 *
	 * @param \Magento\Backend\App\Action\Context                           $context
	 * @param \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection $subscriberCollection
	 * @param \Hellodialog\Base\Model\Hellodialog                           $helloDialog
	 * @param \Hellodialog\Base\Model\Adapter\SubscriberAdapter             $subscriberAdapter
	 * @param \Hellodialog\Base\Model\Adapter\ContactAdapter                $contactAdapter
	 * @param \Hellodialog\Base\Model\Subscriber                            $subscriber
	 * @param \Hellodialog\Base\Model\Config		                        $config
	 * @param \Hellodialog\Base\Helper\Connector\HellodialogConnector       $hdConnector
	 */
	public function __construct(
		Context $context,
		SubscriberCollection $subscriberCollection,
		Hellodialog $helloDialog,
		SubscriberAdapter $subscriberAdapter,
		ContactAdapter $contactAdapter,
		Subscriber $subscriber,
		Config $config,
		HellodialogConnector $hdConnector
	) {
		$this->helloDialog = $helloDialog;
		$this->subscriber = $subscriber;
		$this->config = $config;
		$this->subscriberAdapter = $subscriberAdapter;
		$this->contactAdapter = $contactAdapter;
		$this->subscriberCollection = $subscriberCollection;
		$this->hdConnector = $hdConnector;

		$apiKey = $this->config->getApiKey();
		$apiUrl = $this->config->getApiUrl( 'contacts' );

		$this->hdConnector->init( $apiKey, $apiUrl );

		parent::__construct($context);
	}

	public function execute() {
		$helloDialogContacts = $this->getHellodialogContacts();
		$magentoSubscribers  = $this->getMagento2Subscribers();

		$putPostRequestBodies = $this->prepareHDPutPostRequestBodies($helloDialogContacts, $magentoSubscribers);

		$results['m2_hd'] = $this->syncHDWithM2Data($putPostRequestBodies);
		$results['hd_m2'] = $this->syncM2WithHDData($helloDialogContacts);
	}

	private function getHellodialogContacts()
	{
		$params = [
			"condition" => ["email" => "contains"],
			"values" 	=> ["email" => "@" ]
		];

		$response = $this->hdConnector->get(null, $params);

		$helloDialogContacts = json_decode($response->getContent());

		return $helloDialogContacts;
	}

	private function getMagento2Subscribers()
	{
		$subscriberCollection = $this->subscriberCollection->showCustomerInfo()->getItems();

		return $subscriberCollection;
	}

	private function prepareHDPutPostRequestBodies($helloDialogContacts, $magentoSubscribers)
	{
		$putToHD = [];
		$postToHD = [];
		$putPostRequestBodies = [];

		foreach ($magentoSubscribers as $magentoSubscriber) {
			$foundInHellodialog = false;
			foreach ($helloDialogContacts as $helloDialogContact) {
				if($magentoSubscriber->getEmail() === $helloDialogContact->email) {
					$putToHD[]  = $this->contactAdapter->mapSubscriberToHDContact($magentoSubscriber, true);
					$foundInHellodialog = true;
					break;
				}
			}

			if(!$foundInHellodialog) {
				$postToHD[] = $this->contactAdapter->mapSubscriberToHDContact($magentoSubscriber);
			}
		}

		$putPostRequestBodies['put'] = $putToHD;
		$putPostRequestBodies['post'] = $postToHD;

		return $putPostRequestBodies;
	}

	/*
	 * Send Magento 2 subscriber data to Hello Dialog in one batch
	 * */
	private function syncHDWithM2Data($putPostRequestBodies)
	{
		$putRequestBodies = $putPostRequestBodies['put'];
		$postRequestBodies = $putPostRequestBodies['post'];

		$results = [];

		if( !empty($putPostRequestBodies) ) {
			$results['put'] = $this->hdConnector->put('null', $putRequestBodies);
		}

		if( !empty($postRequestBodies) ) {
			$results['post'] = $this->hdConnector->post($postRequestBodies);
		}

		return $results;
	}

	/*
	 * Turn Hello Dialog contacts into subscribers in Magento 2
	 * */
	private function syncM2WithHDData($helloDialogContacts) {
		$contactsAdded = 0;

		foreach ($helloDialogContacts as $contact) {
			$this->subscriber->setId(null); // clear the subscriber id to create a new record

			$subscriberState = $this->subscriberAdapter->getSubscriberState( $contact->_state );
			$this->subscriber->setStatus( $subscriberState );
			$this->subscriber->setHellodialogId( $contact->id );

			$contactsAdded += $this->subscriber->subscribe( $contact->email );
		}

		return $contactsAdded;
	}
}