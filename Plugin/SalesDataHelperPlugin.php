<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Plugin;

use Magento\Sales\Helper\Data as SalesDataHelper;
use Hellodialog\Base\Model\Config;

class SalesDataHelperPlugin
{
	protected $config;

	public function __construct(
		Config $config
	)
	{
		$this->config = $config;
	}

	public function afterCanSendOrderCommentEmail( SalesDataHelper $subject, $result )
	{
		if( $result === false && $this->config->getTransactionalMailEnabledStatus(Config::TRANSACTIONAL_ORDER_UPDATE) ) {
			return true;
		}

		return $result;
	}

	public function afterCanSendShipmentCommentEmail( SalesDataHelper $subject, $result )
	{
		if( $result === false && $this->config->getTransactionalMailEnabledStatus(Config::TRANSACTIONAL_SHIPMENT_UPDATE) ) {
			return true;
		}

		return $result;
	}

	public function afterCanSendInvoiceCommentEmail( SalesDataHelper $subject, $result )
	{
		if( $result === false && $this->config->getTransactionalMailEnabledStatus(Config::TRANSACTIONAL_INVOICE_UPDATE) ) {
			return true;
		}

		return $result;
	}

	public function afterCanSendCreditmemoCommentEmail( SalesDataHelper $subject, $result )
	{
		if( $result === false && $this->config->getTransactionalMailEnabledStatus(Config::TRANSACTIONAL_CREDIT_MEMO_UPDATE) ) {
			return true;
		}

		return $result;
	}
}