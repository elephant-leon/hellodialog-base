<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Plugin;

use Hellodialog\Base\Model\Hellodialog;
use Hellodialog\Base\Model\Subscriber;
use Hellodialog\Base\Model\Logger;

class SubscriberPlugin
{
	protected $logger;
	protected $helloDialog;
	protected $subscriberModel;

	public function __construct(
        Logger $logger,
        Hellodialog $helloDialog,
		Subscriber $subscriberModel
	)
	{
		$this->logger = $logger;
		$this->helloDialog = $helloDialog;
		$this->subscriberModel = $subscriberModel;
	}

	public function beforeSave(Subscriber $subscriber)
	{
        $subscriber = $this->helloDialog->processSubscriberForHD($subscriber);

		return [$subscriber];
	}
}