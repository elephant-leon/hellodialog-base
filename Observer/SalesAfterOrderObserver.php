<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

namespace Hellodialog\Base\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;

use Hellodialog\Base\Model\Hellodialog;

class SalesAfterOrderObserver implements ObserverInterface
{
	protected $hellodialog;

    /**
     * SalesAfterOrderObserver constructor.
     * @param Hellodialog $hellodialog
     */
	public function __construct(
		Hellodialog $hellodialog
	)
	{
		$this->hellodialog = $hellodialog;
	}

	public function execute( Observer $observer )
	{
		/** @var Order $order */
		$order = $observer->getEvent()->getOrder();

		$order = $order->loadByIncrementId( $order->getIncrementId() );

		$this->hellodialog->processOrderForHD( $order );
	}
}