<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Hellodialog\Base\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Framework\App\State;
use Hellodialog\Base\Cron\ExportSubscriberOrders\Proxy as ExportSubscriberOrders;

class ProcessSubscriberOrdersCommand extends Command {
	const COMMAND_NAME = 'hellodialog:subscriber:orders';
	const COMMAND_DESC = 'Start subscriber order processing.';
	const AREA_CODE = 'adminhtml'; # can be adminhtml or frontend

    /** @var ExportSubscriberOrders  */
	protected $exportSubscriberOrders;

    /**
     * ProcessSubscriberOrdersCommand constructor.
     *
     * @param State $state
     * @param ExportSubscriberOrders $exportSubscriberOrders
     */
	public function __construct(
		State $state,
        ExportSubscriberOrders $exportSubscriberOrders
	)
	{
		$this->exportSubscriberOrders = $exportSubscriberOrders;

		parent::__construct(self::COMMAND_NAME);
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this->setName(self::COMMAND_NAME)
			->setDescription(self::COMMAND_DESC);

		parent::configure();
	}
	
	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
        $this->exportSubscriberOrders->execute();
    }
}