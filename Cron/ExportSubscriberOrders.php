<?php
/**
 * Copyright © 2019 Zeo BV. All rights reserved.
 * @Author Gydo Broos
 * This module was developed by Zeo BV. on behalf of Hellodialog BV. all files in this module are subject to the MIT license.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/
namespace Hellodialog\Base\Cron;


use Hellodialog\Base\Model\Logger as HellodialogLogger;
use Hellodialog\Base\Model\Hellodialog;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory;

class ExportSubscriberOrders {
	/** @var Hellodialog  */
	protected $hellodialog;
	/** @var Subscriber */
	protected $subscriber;
	/** @var CollectionFactory */
	protected $subscriberCollectionFactory;
	/** @var HellodialogLogger  */
	protected $logger;

    /**
     * ProcessCarts constructor.
     *
     * @param Hellodialog $hellodialog
     * @param Subscriber $subscriber
     * @param HellodialogLogger $logger
     */
	public function __construct(
		Hellodialog $hellodialog,
		Subscriber $subscriber,
        CollectionFactory $subscriberCollectionFactory,
		HellodialogLogger $logger
	)
	{
		$this->hellodialog = $hellodialog;
		$this->subscriber = $subscriber;
		$this->subscriberCollectionFactory = $subscriberCollectionFactory;
		$this->logger = $logger;
	}

	public function execute()
	{
        $subscriberCollection = $this->subscriberCollectionFactory->create();

        $this->logger->log( __('Starting export of legacy orders for %1 subscribers', [$subscriberCollection->count()]) );

        foreach ($subscriberCollection as $subscriber) {
            if( !$subscriber->getOrdersExported() ) {
                $this->logger->log( __('Start export for subscriber with id %1', [$subscriber->getHellodialogId()]) );

                $this->hellodialog->processOrdersForHD($subscriber);
                $this->logger->log( __('Done.', [$subscriber->getHellodialogId()]) );

                $subscriber->setOrdersExported(1);

                $subscriber->save();
            } else {
                $this->logger->log( __('Skipping export for subscriber with id %1. Orders already exported.', [$subscriber->getHellodialogId()]) );
            }
        }
	}
}